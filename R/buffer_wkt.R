#' Buffer wkt
#'
#' @param wkt well known text for feature
#' @param buffer_m buffer distance in metres
#'
#' @return mutlipolygon buffer simple feature
#' @export
#'
#' @examples buffer_wkt("POINT(55244 71132)", 2000)
buffer_wkt <- function(wkt, buffer_m){

  df <- as.data.frame(wkt)
  df <- sf::st_as_sf(df, wkt = "wkt", crs = 27700)
  buffer <- sf::st_buffer(df, buffer_m)
  buffer <- sf::st_cast(buffer, "MULTIPOLYGON")

  return(buffer)

}

