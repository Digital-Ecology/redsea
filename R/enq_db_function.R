#' Return search area and centroid for enquiry
#'
#' @param enq_no The enquiry no of the enquiry
#'
#' @return a simple features search area for the given enquiry id
#' @export
#'
#' @examples \dontrun{enq_db_func(6)}

enq_db_func <- function(enq_no){

  enq_query <- "SELECT * FROM postgis.enquiries WHERE enquiries.enq_no = ?enq_no"

  safe_query <- DBI::sqlInterpolate(pool, enq_query, enq_no = enq_no)

  enq_df <- sf::st_read(pool, query = safe_query)

  return(enq_df)

}

