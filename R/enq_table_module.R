#' Shiny output module for a table of enquiries
#'
#' @param id an id
#'
#' @return a DT table
#' @export
#'
#' @examples \dontrun{enquiryTableOutput(id)}

enquiryTableOutput <- function(id){

  ns <- shiny::NS(id)

  DT::dataTableOutput(ns("enq_table"))

}

# server

#' Shiny server module for enquiry tables
#'
#' @param id an id
#'
#' @return a shiny server module
#' @export
#'

enquiryTableServer <- function(id){

  shiny::moduleServer(

    id,

    function(input, output, session){

    enq <- DBI::dbGetQuery(pool, "SELECT
                            e.enq_no,
                            e.enq_date,
                            e.enquirer,
                            e.contact,
                            e.site_name,
                            e.gridref,
                            e.buffer_m,
                            e.start_year,
                            e.end_year,
                            e.spp_filter,
                            e.lws,
                            e.rigs,
                            e.habitats
                            FROM postgis.enq_no_geom e WHERE e.enq_date::date > current_date - interval '18 months' ORDER BY e.search_completed, e.enq_date DESC")

    #old
    #output$enq_table <- DT::renderDT(enq)

    enq$enq_no <- paste0("<a href='#' onclick=\"Shiny.setInputValue('selected_enq_no', '", enq$enq_no, "', {priority: 'event'}); Shiny.setInputValue('tab_selected', 'Search');\">", enq$enq_no, "</a>")

    output$enq_table <- DT::renderDT({
      DT::datatable(
        setNames(enq, c(
          "Enquiry Ref",
          "Enquiry Date",
          "Enquirer",
          "Contact",
          "Site Name",
          "Grid Reference",
          "Buffer (m)",
          "Start Year",
          "End Year",
          "Species Filter",
          "LWS",
          "RIGS",
          "Habitats"
        )),
        escape = FALSE
      )
    })


    }
  )
}
