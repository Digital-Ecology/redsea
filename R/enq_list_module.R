#' Enquiry list shiny ui module
#'
#' @param id an id
#'
#' @return a shiny ui module
#' @export
#'
#' @examples \dontrun{
#' EnqListInput(id)
#' }

EnqListInput <- function(id){

  ns <- shiny::NS(id)
  htmltools::tagList(

  shiny::selectInput(ns("next_enq"), label = "Next Enquiry", choices = 1),

  shiny::actionButton(ns("refresh_enq"), label = "Refresh enquiries", icon = shiny::icon("redo")),

  shiny::h2("Enquiry details"),

  shiny::textOutput(ns("enq_id")),
  shiny::textOutput(ns("enq_date")),
  shiny::textOutput(ns("enquirer")),
  shiny::textOutput(ns("site_name")),
  shiny::textOutput(ns("grid_ref")),
  shiny::textOutput(ns("buffer")),
  shiny::textOutput(ns("spp_filter")),
  shiny::textOutput(ns("lws")),
  shiny::textOutput(ns("rigs")),
  shiny::textOutput(ns("habitats")),
  shiny::br(),
  shiny::actionButton(ns("searchbtn"), label = "Search!", icon = icon("search"))
  )}


#' Enquiry list shiny server module
#'
#' @param id an id
#'
#' @return a shiny server module
#' @export
#'
#' @examples \dontrun{enq_list_server(id)}

enq_list_server<- function(id){

  shiny::moduleServer(

    id,

    function(input, output, session){

      shiny::observeEvent(input$refresh_enq, {

        enq_list <- DBI::dbGetQuery(pool, "SELECT * FROM postgis.enq_no_geom WHERE search_completed = false ORDER BY id")

        shiny::updateSelectInput(session, "next_enq", choices = enq_list$id)
        })

# update list of enquiries

      shiny::observe(label = "enq_filter_observer", {

        x <- input$next_enq

        query <- "SELECT * FROM postgis.enq_no_geom WHERE id = $1 ORDER BY id"

        # fetch enquiry from database
        enq_list <- DBI::dbGetQuery(pool, query, params = list(x))

        output$enq_id <- shiny::renderText({paste("Enquiry ID: ", enq_list$id)})
        output$enq_date <- shiny::renderText({paste("Enquiry date: ",enq_list$enq_date)})
        output$enquirer <- shiny::renderText({paste("Enquirer: ", enq_list$enquirer)})
        output$site_name <- shiny::renderText({paste("Site name: ", enq_list$site_name)})
        output$grid_ref <- shiny::renderText({paste("Grid reference: ", enq_list$gridref)})
        output$buffer <- shiny::renderText({paste("Search buffer (m): ", enq_list$buffer_m)})
        output$spp_filter <- shiny::renderText({paste("Species filter: ", enq_list$spp_filter)})
        output$lws <- shiny::renderText({paste("Include LWS?: ", enq_list$lws)})
        output$rigs <- shiny::renderText({paste("Include RIGS?: ", enq_list$rigs)})
        output$habitats <- shiny::renderText({paste("Include habitats?: ", enq_list$habitats)})
        })

      })}
