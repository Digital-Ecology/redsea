#' Sites search function
#'
#' @param enq_no enquiry no of the enquiry
#'
#' @return simple features data set of sites
#' @export
#'
#' @examples \dontrun{site_db_function(6)}

site_db_func <- function(enq_no){

  enq_no <- enq_no

  site_query <- "
    SELECT
      postgis.lws.siteid AS site_code,
      postgis.lws.sitename AS site_name,
      postgis.lws.type AS designation,
      postgis.lws.geom
    FROM
      postgis.lws, postgis.enquiries
    WHERE
      ST_Intersects(postgis.lws.geom, postgis.enquiries.geometry)
      AND postgis.enquiries.enq_no = ?enq_no
      AND postgis.enquiries.lws IS TRUE
    UNION
    SELECT
      postgis.rigs.siteid AS site_code,
      postgis.rigs.sitename AS site_name,
      postgis.rigs.type AS designation,
      postgis.rigs.geom
    FROM
      postgis.rigs, postgis.enquiries
    WHERE
      ST_Intersects(postgis.rigs.geom, postgis.enquiries.geometry)
      AND postgis.enquiries.enq_no = ?enq_no
      AND postgis.enquiries.rigs IS TRUE"

  safe_query <- DBI::sqlInterpolate(pool, site_query, enq_no = enq_no)

  site_df <- sf::st_read(pool, query = safe_query)

  sf::st_crs(site_df) <- 27700

  return(site_df)

}
