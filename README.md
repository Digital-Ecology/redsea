<!-- README.md is generated from README.Rmd. Please edit that file -->

# redsea

<!-- badges: start -->
<!-- badges: end -->

RedSea is a the data search app for RECORD, the Local Environmental
Records Centre for the Cheshire Region.

It has been designed to query a database of biological records, sites
and habitat data in response to data search enquiries submitted to
RECORD.

The app requires access to a database to be able to perform searches.
Without it, the app will fail on start-up.

## Installation

You can install the development version of redsea like so:

``` r
if (!require("remotes"))
remotes::install_git("https://codeberg.org/Digital-Ecology/redsea.git")
```

## Example

To run the app locally, you can use the `run_app()` function to load the
shiny app.

``` r
library(redsea)
run_app()
```

However, you will need to have a database to connect to.

The app has the following functionality:

- add new enquiries using a form, and delete enquiries as required.
- select enquiries and carry out searches of the database using spatial
  intersects with the search area
- review species data returned by the search and edit it as necessary.
- generate a data search report. This is an interactive html document. A
  webmap is also created displaying all of the data returned by the
  search.

## Adapting for your LERC

If you want to use this app for your own LERC data searches you will
need:

- your own database (see the wiki for details)
- to adapt the report template to meet your needs

The app can run locally if provide with the database credentials as a
standalone shiny app. It can also be deployed on a webserver using
Docker.

If you would like help in deploying your own version, please get in
touch with Digital Ecology: <info@digital-ecology.co.uk>
