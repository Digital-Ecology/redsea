# Base image https://hub.docker.com/u/rocker/
FROM rocker/shiny:latest

# system libraries of general use
## install debian packages
RUN apt-get update -qq && apt-get -y --no-install-recommends install \
    libxml2-dev \
    libcairo2-dev \
    libsqlite3-dev \
    libmariadbd-dev \
    libpq-dev \
    libssh2-1-dev \
    unixodbc-dev \
    libcurl4-openssl-dev \
    libssl-dev \
    libudunits2-0 \
    libproj22 \
    libgdal30

## update system libraries
RUN apt-get update && \
    apt-get upgrade -y && \
    apt-get clean

# Copy over just the files required to install the dependencies, and then install them. We do this
# before copying over any other source files so that the layer can be cached and doesn't need to be
# regenerated every time a source file changes.
COPY redsea.Rproj /app/
COPY DESCRIPTION /app/

# Install dependencies for app.
WORKDIR /app
RUN R -e "install.packages('remotes'); remotes::install_deps('.')"

# Copy the source files to /app and install the local package.
COPY . /app
RUN R -e "remotes::install_local('.', dependencies = TRUE)"

# Run the app.
CMD R -e "require('redsea'); run_app()"
